import { Component, OnInit } from '@angular/core';
import {StudentService} from '../services/student.service';
@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {

  student:any={};
  studentList:any=[];
  constructor(private studentService: StudentService) { }

  ngOnInit(): void {
  	this.getStudentList();
  }

  getStudentList(){
  	this.studentList=[];
  	this.studentService.getStudentList().then(res=>{
  		console.log(res);
  		this.studentList=res;
  	}),(err=>{
  		console.log(err);
  	})
  }
  saveStudent(){
  	console.log(this.student)
  	this.studentService.createStudent(this.student).then(res=>{
  		console.log(res);
  		this.getStudentList();
  	}),(err=>{
  		console.log(err);
  	})
  }

  addStudent(){
  	this.student={};
  }
}
