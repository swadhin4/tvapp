import { Inject, Injectable } from '@angular/core';
import { LOCAL_STORAGE, StorageService } from 'ngx-webstorage-service';
const STORAGE_KEY=null
@Injectable({
  providedIn: 'root'
})
export class LocalstorageService {
  
  constructor(@Inject(LOCAL_STORAGE) private storage: StorageService) { }

     public storeUser(user: any): void {
          this.storage.set(STORAGE_KEY, user);
          console.log(this.storage.get(STORAGE_KEY) || 'LocaL storage is empty');
     }

      public getUser(): void {
          let user= this.storage.get(STORAGE_KEY);
          if(user!=null){
          	return user;
          }else{
          	return null;
          }
     }

     public logOff():void {
        let user= this.storage.get(STORAGE_KEY);
          if(user!=null){
             return this.storage.remove(STORAGE_KEY);
          }
     }
}
