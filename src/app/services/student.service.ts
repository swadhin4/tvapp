import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import * as API from '../constants/endpoint'; 
@Injectable({
  providedIn: 'root'
})
export class StudentService {

  constructor(private http: Http) { }

  	getStudentList(){
  		return this.getApiResponse(API.LOCAL_SERVER +"/students");
  	}

  	createStudent(student:any){ 
		return this.postApiResponse(API.LOCAL_SERVER +"/students", student);
  	}

  	updateStudent(){

  	}

  	deleteStudent(){

  	}

    getApiResponse(api: string){
		return new Promise((resolve, reject) => {
		let headers = new Headers();
		headers.append('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, DELETE, PUT');
		this.http.get(api)
		  .subscribe(res => {
			   resolve(res.json());
			   let responseObj =  res.json();
			   console.log(responseObj);
		  }, (err) => {
			reject(err);
		  });
	  });
	}

	postApiResponse(api: string, payLoad: any) {
		return new Promise((resolve, reject) => {
			this.http.post(api, payLoad)
				.subscribe(res => {
					resolve(res.json());
					let responseObj = res.json();
					console.log(responseObj);
				}, (err) => {
					reject(err);
				});
		});
	}
}
