import { Component, OnInit,ViewChild,AfterViewInit } from '@angular/core';
import {SortPipe} from './../constants/sort.pipe';

import { MatTableModule,MatTableDataSource } from "@angular/material/table";
import { MatSort} from "@angular/material/sort";
import { MatPaginator} from "@angular/material/paginator";

export interface UserData {
  id: string;
  name: string;
  progress: string;
  color: string;
}
const COLORS = ['maroon', 'red', 'orange', 'yellow', 'olive', 'green', 'purple',
  'fuchsia', 'lime', 'teal', 'aqua', 'blue', 'navy', 'black', 'gray'];

  
const NAMES = ['Maia', 'Asher', 'Olivia', 'Atticus', 'Amelia', 'Jack',
  'Charlotte', 'Theodore', 'Isla', 'Oliver', 'Isabella', 'Jasper',
  'Cora', 'Levi', 'Violet', 'Arthur', 'Mia', 'Thomas', 'Elizabeth'];

@Component({
  selector: 'app-table-list',
  templateUrl: './table-list.component.html',
  styleUrls: ['./table-list.component.css']
})
export class TableListComponent implements AfterViewInit {

   users:any=[];
   displayedColumns = ['id', 'name', 'progress', 'color'];
   direction:string="asc"
   column:string="name";
   type:string="string";

  dataSource: MatTableDataSource<UserData>;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  constructor(private sortPipe:SortPipe) { }

 
  setSortParams(direction,column, type){
	 this.direction=direction
   this.column=column;
   this.type=type;
	 this.sortPipe.transform(this.users,this.direction, this.column, this.type)
	}

 getRandomString(length) {
    var randomChars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    var result = '';
    for ( var i = 0; i < length; i++ ) {
        result += randomChars.charAt(Math.floor(Math.random() * randomChars.length));
    }
    return result;
 }

 randomInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
 }

 getRandomCities(){
 	const cities = ["Delhi", "Hyderabad", "Bhubaneswar", "Mumbai", "Kolkatta", "Chandigarh", "Pune"];
 	const randomElement = cities[Math.floor(Math.random() * cities.length)];
 	return randomElement;
 }
  populateData(){
  	const users: UserData[] = [];
    for (let i = 1; i <= 100; i++) { users.push(this.createNewUser(i)); }

    // Assign the data to the data source for the table to render
    this.dataSource = new MatTableDataSource(users);
  }

  ngAfterViewInit() {
    this.populateData();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }


/** Builds and returns a new User. */
 createNewUser(id: number): UserData {
  const name =
      NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
      NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

  return {
    id: id.toString(),
    name: name,
    progress: Math.round(Math.random() * 100).toString(),
    color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
  };
}

}
