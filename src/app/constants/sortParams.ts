import {Directive,Output,EventEmitter,ElementRef,HostListener} from '@angular/core';

@Directive({selector: "[appSortParams]"})

export class SortParamsDirective {
@Output() param:EventEmitter<any>=new EventEmitter();
constructor(private element:ElementRef) { }

@HostListener('click') onClickIcon(){
this.selectSort(this.element.nativeElement.id)
}

selectSort(id){
switch(id){
	case "cityAsc":
	this.param.emit({dir:"asc",col:"city",typ:"string"})
	break;
	case "salaryAsc":
	this.param.emit({dir:"asc",col:"salary",typ:"number"})
	break;
	case "nameAsc":
	this.param.emit({dir:"asc",col:"name",typ:"string"})
	break;
	case "cityDesc":
	this.param.emit({dir:"desc",col:"city",typ:"string"})
	break;
	case "salaryDesc":
	this.param.emit({dir:"desc",col:"salary",typ:"number"})
	break;
	case "nameDesc":
	this.param.emit({dir:"desc",col:"name",typ:"string"})
	break;
}}}