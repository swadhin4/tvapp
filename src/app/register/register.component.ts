import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators,ReactiveFormsModule  } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user:any={};
  public registerForm: FormGroup;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit() {
   this.registerForm = this.formBuilder.group({
      'mobile': ['', Validators.compose([Validators.maxLength(10),Validators.required ])],     
      'password': ['', Validators.compose([Validators.required ])],
      'confirmPassword': ['', Validators.compose([Validators.required ])]		     
    }); 
  }

  onSubmit(registerForm){
  	console.log(registerForm)
  }

}
