import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators,ReactiveFormsModule  } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalstorageService } from './../services/local-storage.service';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
}) 
export class LoginComponent implements OnInit {
  user:any={};
  public onLoginForm: FormGroup;
  isValid=false;

  constructor(private formBuilder: FormBuilder,private router:Router) { }

  ngOnInit() {
   this.onLoginForm = this.formBuilder.group({
      'mobile': ['', Validators.compose([Validators.maxLength(10),Validators.required ])],     
      'password': ['', Validators.compose([Validators.required ])]	     
    }); 
  }
 
 onSubmit(loginForm){
	 console.log(loginForm);
  	if(loginForm.valid){
    	console.log("OK")
  		this.user=loginForm.value;
		  this.router.navigate(['home'])
  	}
  }
}
