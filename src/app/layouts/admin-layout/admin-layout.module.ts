import { NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UserProfileComponent } from '../../user-profile/user-profile.component';
import { TableListComponent } from '../../table-list/table-list.component';
import { TypographyComponent } from '../../typography/typography.component';
import { IconsComponent } from '../../icons/icons.component';
import { MapsComponent } from '../../maps/maps.component';
import { NotificationsComponent } from '../../notifications/notifications.component';
import { UpgradeComponent } from '../../upgrade/upgrade.component';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatRippleModule} from '@angular/material/core';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatSelectModule} from '@angular/material/select';
import {MatCardModule} from '@angular/material/card';
import { HomeComponent } from '../../home/home.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SearchComponent } from './../../components/search/search.component';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { StudentComponent } from '../../student/student.component';
import { TeacherComponent } from '../../teacher/teacher.component';
import { BarComponent } from '../../bar/bar.component';

import {SortPipe} from '../../constants/sort.pipe';
import {SortParamsDirective} from '../../constants/sortParams';
import { MatTableModule } from "@angular/material/table";
 import {  MatSortModule} from "@angular/material/sort";
 import { MatPaginatorModule} from "@angular/material/paginator";
  import { MatProgressSpinnerModule} from "@angular/material/progress-spinner";
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatCardModule,
    MatAutocompleteModule,
    MatTableModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    MatSortModule,
    MDBBootstrapModule.forRoot()
  ],
  declarations: [
    HomeComponent,
    SearchComponent,
    DashboardComponent,
    UserProfileComponent,
    TableListComponent,
    TypographyComponent,
    IconsComponent,
    MapsComponent,
    NotificationsComponent,
    UpgradeComponent,
    StudentComponent,
    TeacherComponent,
    BarComponent
  ],
  providers:[ SortPipe,SortParamsDirective],
  schemas: [
     CUSTOM_ELEMENTS_SCHEMA
   ],
})

export class AdminLayoutModule {}
